library ieee;
use ieee.std_logic_1164.all;

entity top_ticks_tb is
end top_ticks_tb;

architecture Behavioral of top_ticks_tb is

    component top is
        port (segment_t : out std_logic_vector (6 downto 0);
              CLK_t     : in std_logic;
              BOTON_t   : in std_logic_vector (3 downto 0);
              SW_ON_t   : in std_logic
             
              --botones : out std_logic_vector(3 downto 0)
                );
    end component;

    signal segment_t : std_logic_vector (6 downto 0);
    signal CLK_t     : std_logic;
    signal BOTON_t   : std_logic_vector (3 downto 0);
    signal SW_ON_t   : std_logic;
    
    
    constant CLK_prueba: time := 10 sec;
begin

    uuut : top
    port map (segment_t => segment_t,
              CLK_t     => CLK_t,
              BOTON_t   => BOTON_t,
              SW_ON_t   => SW_ON_t
             
             -- botones =>botones
              );

 --reloj
 clkgen: process
  begin
  	CLK_t <= '0';
    wait for 0.000005*CLK_prueba;
    CLK_t <= '1';
    wait for 0.000005 * CLK_prueba;
  end process;
  
   
 tester: process
  begin
   BOTON_t<="0000";
   
   SW_ON_t<='0';
    wait for 0.5* CLK_prueba;
    SW_ON_t <= '1';--encendemos la cafetera
 
  wait for 0.5 * CLK_prueba;
   BOTON_t(3)<='1'; --pedimos  leche
  wait for 0.05 * CLK_prueba;
   Boton_t(3)<='0';
   
--	wait for 3.5 * CLK_prueba;   
--   Boton_t(1)<= '1'; --pedimos  un corto con leche
--   wait for 0.05 * CLK_prueba;
--   Boton_t(1)<='0';
   
--    wait for 1*CLK_prueba;
    
--    Boton_t(0)<='1'; --pedimos un largo sin elegir liquido luego no deberia de ir
--      wait for 0.05 * CLK_prueba;
--    Boton_t(0)<='0';
    
    
--    wait for 0.5 * CLK_prueba;
--    Boton_t(2)<= '1'; --pedimos  agua
--      wait for 0.05 *CLK_prueba;
--    Boton_t(2)<='0';
     
--     wait for 1*CLK_prueba;--Si en vez de 1*CLK ponemos 1.5*CLK no funciona: se supera el tiempo de elecci�n y hay que volver a elegir liquido     
--    Boton_t(0)<='1'; --pedimos un largo con agua
--  wait for 0.05 * CLK_prueba;
--    Boton_t(0)<='0';
   	
--    wait for 2* CLK_prueba;
 	SW_ON_t<= '0';  
  
    wait for 0.05* CLK_prueba;
 
    wait for 3 * CLK_prueba;
    
    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
  end process;

end architecture;

-- Configuration block below is required by some simulators. Usually no need to edit.
