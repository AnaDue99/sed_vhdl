library ieee;
use ieee.std_logic_1164.all;

entity decoder_tb is
end decoder_tb;

architecture Behavioral of decoder_tb is

    component decoder is
        port (code : IN std_logic_vector(35 DOWNTO 0);
             display : OUT std_logic_vector(6 DOWNTO 0)
          
                );
    end component;

    signal code : std_logic_vector (35 downto 0);
    signal display     : std_logic_vector(6 DOWNTO 0);
  
    constant CLK_PERIOD: time:= 100ns;

begin

    uuut : decoder
    port map (code=> code,
              display=>display
              );

 --reloj
 
 tester: process
  begin
    
    
    wait for 5 * CLK_PERIOD;
   code<= X"01D34CE80"; --4,9
      wait for 0.5 * CLK_PERIOD;
   code<= X"00DB58580"; --2,3
   wait for 0.5 * CLK_PERIOD;
   code<= X"033428F00"; --8,6
   wait for 0.5 * CLK_PERIOD;
   code<= X"02A51BD80"; --7,1
   wait for 0.5 * CLK_PERIOD;
   code<= X"013112D00"; --0,2
 
      
    
    
    
    wait for 5 * CLK_PERIOD;
    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
  end process;

end architecture;
