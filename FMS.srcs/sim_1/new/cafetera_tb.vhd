-- Code your testbench here
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity CAFETERA_tb is
end entity;

architecture test of CAFETERA_tb is

  -- Inputs
  signal BOTON : std_logic_vector(3 downto 0);
  signal SW_ON: std_logic;
  signal  CLK : std_logic;
  -- Outputs
  signal LED: std_logic_vectoR(6 downto 0);
  signal cuenta_atras: std_logic_vector(35 downto 0);

component CAFETERA is
    port (
  --	RESET : in std_logic;
 	CLK : in std_logic;
 	BOTON : in std_logic_vector(3 downto 0);
 	SW_ON: in std_logic;
 	LED: out std_logic_vector(6 downto 0);
 	cuenta_atras: out std_logic_vector(35 downto 0)
 );

  end component;

  constant CLK_PERIOD : time := 10 sec ;  -- Clock period 
 

begin
  -- Unit Under Test
  uut: CAFETERA
     port map (
     
     Boton       => Boton,
     LED         => LED,
     SW_ON       => SW_ON,
  --   RESET       => RESET,
     CLK         => CLK,
     cuenta_atras => cuenta_atras
    );
    
    
             
  clkgen: process
  begin --simulacion de un reloj de 0.1Mhz
  	CLK <= '0';
    wait for 0.000005 * CLK_PERIOD;
    CLK <= '1';
    wait for 0.000005 * CLK_PERIOD;
  end process;

--RESET <= '1',
  --       '0' after 0.25 * CLK_PERIOD;        
  
  tester: process
  begin
   BOTON<="0000";
   SW_ON<='0';
    wait for 0.5* CLK_PERIOD;
    SW_ON <= '1';--encendemos la cafetera
    
  wait for 0.5 * CLK_PERIOD;
   BOTON(3)<='1'; --pedimos  leche
  wait for 0.05 * CLK_PERIOD;
   Boton(3)<='0';
   
	wait for 0.5 * CLK_PERIOD;   
   Boton(1)<= '1'; --pedimos  un corto con leche
   wait for 0.05 * CLK_PERIOD;
   Boton(1)<='0';
   
    wait for 1*CLK_PERIOD;
    
    Boton(0)<='1'; --pedimos un largo sin elegir liquido luego no deberia de ir
      wait for 0.05 * CLK_PERIOD;
    Boton(0)<='0';
    
    
    wait for 0.5 * CLK_PERIOD;
    Boton(2)<= '1'; --pedimos  agua
      wait for 0.05 * CLK_PERIOD;
    Boton(2)<='0';
     
     wait for 1*CLK_PERIOD;--Si en vez de 1*CLK ponemos 1.5*CLK no funciona: se supera el tiempo de elecci�n y hay que volver a elegir liquido     
    Boton(0)<='1'; --pedimos un largo con agua
  wait for 0.05 * CLK_PERIOD;
    Boton(0)<='0';
   	
    wait for 2 * CLK_PERIOD;
 	SW_ON<= '0';  
    
    wait for 2 * CLK_PERIOD;
    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
  end process;
  
  

end architecture;