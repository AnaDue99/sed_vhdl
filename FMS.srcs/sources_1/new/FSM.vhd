library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;


entity CAFETERA is
 port (

 	CLK : in std_logic;
 	BOTON : in std_logic_vector(3 downto 0):="0000";
 	SW_ON: in std_logic;
 	--Boton 3: leche
 	--Boton 2: agua
 	--Boton 1: corto
	 --Boton 0: largo
 	LED: out std_logic_vector(6 downto 0);
    --Led 3: leche
	--Led 2: agua
 	--Led 1: corto
 	--Led 0: largo
 	--Led 4: te
 	--Led 5: colacao
	--Led 6: on
	
    cuenta_atras: out std_logic_vector(35 downto 0):=X"000000000"
 );
 
end CAFETERA;

architecture behavioral of CAFETERA is
 		type estate_t is (S0_OFF, S1_ELEGIR_LIQ, S2_AGUA, S3_LECHE ,S4_AGUA_CORTO,S5_AGUA_LARGO, S6_LECHE_CORTO, S7_LECHE_LARGO, S8_COLA_CAO ,S9_TE );
 		signal estate,nxt_state: estate_t;
        signal remaining_ticks, nxt_remaining_ticks : unsigned (35 downto 0):=X"000000000"; 
       constant timer_max :   unsigned := X"01DCD6500" ;  --500*10^6 
       constant timer_corto : unsigned := X"029B92700";  --700*10^6
       constant timer_largo : unsigned := X"035A4E900"; --900*10^6
       constant timer_te : unsigned := X"023C34600";  --600*10^6
       constant timer_cola_cao : unsigned := X"02FAF0800"; --800*10^6
     
begin
 
 state_register: process (CLK)
 	begin 		 
  
  if (clk'event and clk='1') then
    		estate<=nxt_state;
    		remaining_ticks <= nxt_remaining_ticks;
             if remaining_ticks /= 0 then
                cuenta_atras<= std_logic_vector(remaining_ticks-1);
             elsif remaining_ticks = 0 then
                cuenta_atras<=X"000000000";
            end if;         
    end if;
   
 end process;

 nxtstate_decod: process (BOTON,estate,remaining_ticks,SW_ON)
	begin 
	
	nxt_state <= estate;
	
	if SW_ON ='0' then
            nxt_state<= S0_OFF;
            nxt_remaining_ticks<=X"000000000";   
    else 
 	case estate is
     	when S0_OFF =>
      		if SW_ON = '1' then
        		nxt_state <= S1_ELEGIR_LIQ;         	 	
       		end if;
       
 	 	when S1_ELEGIR_LIQ =>  --Si est� encendido
          	
          	if BOTON(3)= '1' then --ponemos leche                 
                nxt_state <= S3_LECHE;
               	nxt_remaining_ticks <= timer_max; -- tiempo de espera para elegir si corto o largo
          	elsif BOTON(2)= '1' then --ponemos agua              
               nxt_state <= S2_AGUA;
               nxt_remaining_ticks <= timer_max; -- tiempo de espera para elegir si corto o largo
             
             elsif BOTON(1)= '1' then --ponemos colacao            
               nxt_state <= S8_COLA_CAO;
               nxt_remaining_ticks <= timer_cola_cao; -- tiempo de espera para elegir si corto o largo
             
             elsif BOTON(0)= '1' then --ponemos te             
               nxt_state <= S9_TE;
               nxt_remaining_ticks <= timer_te; -- tiempo de espera para elegir si corto o largo
             
            end if; 
          
          when S2_AGUA =>  --Si se ha elegido agua
            if BOTON(1)= '1' then -- ponemos corto                  
        		nxt_state <=  S4_AGUA_CORTO;             
               nxt_remaining_ticks <= timer_corto; --tiempo de espera para obtener el caf�
                   
                  
          elsif BOTON(0)= '1' then --ponemos largo                 
                   	nxt_state <=   S5_AGUA_LARGO;
                    nxt_remaining_ticks <= timer_largo;--tiempo de espera para obtener el caf�
           		
   		  elsif remaining_ticks = 1 then
                   	 nxt_state <= S1_ELEGIR_LIQ;
                                             
          else
         			nxt_remaining_ticks <= remaining_ticks - 1;          
               
          end if;
          
          when S3_LECHE =>  --Si se ha elegido leche 
             if BOTON(1)= '1' then -- ponemos corto   
                nxt_state <=  S6_LECHE_CORTO;             
               nxt_remaining_ticks <= timer_corto; --tiempo de espera para obtener el caf�
                                    
              elsif BOTON(0)= '1' then --ponemos largo                 
                        nxt_state <=   S7_LECHE_LARGO;
                        nxt_remaining_ticks <= timer_largo;--tiempo de espera para obtener el caf�
              elsif remaining_ticks = 1 then
                         nxt_state <= S1_ELEGIR_LIQ;                   
              else
                        nxt_remaining_ticks <= remaining_ticks - 1;          
                   
              end if;
          
          
        when S4_AGUA_CORTO =>  --Si se ha elegido corto con agua
            	if remaining_ticks = 1 then
                   	 nxt_state <= S1_ELEGIR_LIQ;
         		 else
         			 nxt_remaining_ticks <= remaining_ticks - 1;    
               
         		 end if;
          
          when S5_AGUA_LARGO =>  --Si se ha elegido largo con agua
  
         if remaining_ticks = 1 then
                   	 nxt_state <= S1_ELEGIR_LIQ;
          else
         			 nxt_remaining_ticks <= remaining_ticks - 1;      
               
          end if;
          
          when S6_LECHE_CORTO =>  --Si se ha elegido corto con leche
       
         if remaining_ticks = 1 then
                   	 nxt_state <= S1_ELEGIR_LIQ;
          else
         			 nxt_remaining_ticks <= remaining_ticks - 1;      
               
          end if;
          
          when S7_LECHE_LARGO =>  --Si se ha elegido largo con leche     
         if remaining_ticks = 1 then
                   	 nxt_state <= S1_ELEGIR_LIQ;
          else
         			 nxt_remaining_ticks <= remaining_ticks - 1;      
               
          end if;
           when S8_COLA_CAO =>  --Si se ha elegido colacao    
         if remaining_ticks = 1 then
                   	 nxt_state <= S1_ELEGIR_LIQ;
          else
         			 nxt_remaining_ticks <= remaining_ticks - 1;      
               
          end if;
           when S9_TE =>  --Si se ha elegido te      
         if remaining_ticks = 1 then
                   	 nxt_state <= S1_ELEGIR_LIQ;
          else
         			 nxt_remaining_ticks <= remaining_ticks - 1;      
               
          end if;
       end case;
       end if;
   end process;
   
    otput_decod : process(SW_ON,estate)
begin 
	 
    	case estate is
        	when S0_OFF =>
        		LED <= "0000000";
            
        	when S1_ELEGIR_LIQ  =>
        		LED <= "1000000";  
                
        	when  S2_AGUA =>
        		LED <= "1000100";
               
       		when S3_LECHE =>
        			LED <= "1001000";  
            
         	when S4_AGUA_CORTO =>
        		LED <= "1000110";
             
       		 when S5_AGUA_LARGO =>
        		LED <= "1000101";  
         
       		 when S6_LECHE_CORTO =>
        		LED <= "1001010";
            
       		 when S7_LECHE_LARGO =>
        		LED <= "1001001";   
        		
        	 when S8_COLA_CAO =>
        		LED <= "1100000";  
        	 when S9_TE =>
        		LED <= "1010000";  		
        	when others =>
        		 LED <= "0000000";
  	end case;
   
 end process;  
end behavioral;