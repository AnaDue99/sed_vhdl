
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

ENTITY top IS
 PORT (
 --display
    segment_t : OUT std_logic_vector(6 DOWNTO 0);
    digsel_t : IN std_logic_vector(7 downto 0);
    digctrl_t : OUT std_logic_vector(7 downto 0);
 --cafetera(leds)
 	CLK_t : in std_logic;
 	BOTON_t : in std_logic_vector(3 downto 0);
 	SW_ON_t: in std_logic;
 	--Boton 3: leche
 	--Boton 2: agua
 	--Boton 1: corto
	 --Boton 0: largo
 	LED_t: out std_logic_vector(6 downto 0) ;
	
	--BORRAR AL IMPLEMENTAR A LA PLACA
botones:out std_logic_vector(3 downto 0);
ticks: out std_logic_vector(35 downto 0)
	
 );
END top;

architecture Behavioral of top is
--se�ales necesarias
 
 signal boton_edge: std_logic_vector(3 downto 0);
 signal code_d: std_logic_vector(35 downto 0);

COMPONENT filtro 
PORT(
clk : in std_logic;
entrada : in std_logic_vector(3 downto 0);
salida : out std_logic_vector(3 downto 0)

);
END COMPONENT;
COMPONENT CAFETERA
PORT (
 --	RESET : in std_logic;
 	CLK : in std_logic;
 	BOTON : in std_logic_vector(3 downto 0);
 	SW_ON: in std_logic;
 	--Boton 3: leche
 	--Boton 2: agua
 	--Boton 1: corto
	 --Boton 0: largo
 	LED: out std_logic_vector(6 downto 0);
   
	
	cuenta_atras: out std_logic_vector (35 downto 0)
);
END COMPONENT;
COMPONENT decoder
PORT (
code : IN std_logic_vector(35 DOWNTO 0);
display : OUT std_logic_vector(6 DOWNTO 0);
digctrl:OUT std_logic_vector(7 DOWNTO 0);
digsel:IN std_logic_vector(7 DOWNTO 0);
sw_on:IN std_logic
);
END COMPONENT;

   
begin



inst_CAFETERA: CAFETERA PORT MAP(

SW_ON=>SW_ON_t,
CLK=>CLK_t,
BOTON=>boton_edge,
LED=>LED_t,
cuenta_atras=>code_d


);
Inst_Display: Decoder PORT MAP (
code => code_d,
display => segment_t,
digctrl => DIGCTRL_t,
digsel => DIGSEL_t,
sw_on => SW_ON_t
);
Inst_Filtro: filtro PORT MAP (
 clk => CLK_t,
 entrada => BOTON_t,
 salida => boton_edge
 
 );

--BORRAR EN CASO DE IMPLEMENTAR EN PLACA
botones<=boton_edge;
ticks<= code_d;
end Behavioral;
