
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity filtro is
PORT(
clk : in std_logic;
entrada : in std_logic_vector(3 downto 0);
salida : out std_logic_vector(3 downto 0)
 );
end filtro;



architecture Behavioral of filtro is
signal btn_sinc: std_logic_vector(3 downto 0);

COMPONENT SYNCHRNZR
PORT (
 CLK : in std_logic;
 ASYNC_IN : in std_logic;
 SYNC_OUT : out std_logic
);
END COMPONENT;

COMPONENT EDGEDTCTR
PORT (
 CLK : in std_logic;
 SYNC_IN : in std_logic;
 EDGE : out std_logic
);
END COMPONENT;
begin
Inst_SYNCHRNZR_btnA: SYNCHRNZR PORT MAP(

 CLK=>clk,
 ASYNC_IN=>entrada(2),
 SYNC_OUT=>btn_sinc(2)--signal

);
Inst_SYNCHRNZR_btnM: SYNCHRNZR PORT MAP(

 CLK=>clk,
 ASYNC_IN=>entrada(3),
 SYNC_OUT=>btn_sinc(3)--signal

);
Inst_SYNCHRNZR_btnL: SYNCHRNZR PORT MAP(

 CLK=>clk,
 ASYNC_IN=>entrada(0),
 SYNC_OUT=>btn_sinc(0)--signal

);
Inst_SYNCHRNZR_btnC: SYNCHRNZR PORT MAP(

 CLK=>clk,
 ASYNC_IN=>entrada(1),
 SYNC_OUT=>btn_sinc(1)--signal

);

Inst_EDGEDTCTR_btnA: EDGEDTCTR PORT MAP(

 CLK=>clk,
 SYNC_IN=>btn_sinc(2),--signal
 EDGE=> salida(2)

);
Inst_EDGEDTCTR_btnM: EDGEDTCTR PORT MAP(

 CLK=>clk,
 SYNC_IN=>btn_sinc(3),--signal
 EDGE=> salida(3)--signal

);
Inst_EDGEDTCTR_btnL: EDGEDTCTR PORT MAP(

 CLK=>clk,
 SYNC_IN=>btn_sinc(0),--signal
 EDGE=>salida(0)

);
Inst_EDGEDTCTR_btnC: EDGEDTCTR PORT MAP(

 CLK=>clk,
 SYNC_IN=>btn_sinc(1),--signal
 EDGE=>salida(1)

);

end Behavioral;
