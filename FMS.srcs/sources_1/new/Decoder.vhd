LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;
ENTITY decoder IS
PORT (
code : IN std_logic_vector(35 DOWNTO 0);
display : OUT std_logic_vector(6 DOWNTO 0):="0000000";
digctrl:OUT std_logic_vector(7 DOWNTO 0);
digsel:IN std_logic_vector(7 DOWNTO 0);
sw_on:IN std_logic
);
END ENTITY decoder;
ARCHITECTURE behavioral OF decoder IS

BEGIN

encendido: process (sw_on)
begin
    if sw_on = '1' then
    digctrl <= not(digsel);
    else
       digctrl<= "11111111";
    
    end if;
end process;

prog: process (code)
 	begin
    if code > X"02FAF0800" then --mayor de 8
    display <= "0000100";--9
    elsif code > X"029B92700" then --mayor de 7
    display <= "0000000";--8
    elsif code > X"023C34600" then --mayor de 6
    display <= "0001111";--7
    elsif code > X"01DCD6500" then --mayor de 5
    display <= "0100000";--6
    elsif code > X"017D78400" then
    display <= "0100100";--5
    elsif code > X"011E1A300" then
    display <= "1001100";--4
    elsif code > X"00BEBC200" then
    display <= "0000110";--3
    elsif code > X"005F5E100" then
    display <= "0010010";--2
    elsif code > X"02FAF080" then
    display <= "1001111";--1
    elsif  code = X"000000000" then
    display <= "0000001";--0
    else 
    display <= "0000001";
end if;
end process;


END ARCHITECTURE behavioral;